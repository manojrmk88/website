import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
 class Game extends React.Component {

   constructor() {
      super();
       this.onMove = this.onMove.bind(this);
      this.state = {
         data:
         [
            {
               "id":1,
               "name":"Foo",
               "age":"20"
            },
            {
               "id":2,
               "name":"Bar",
               "age":"30"
            },
            {
               "id":3,
               "name":"Baz",
               "age":"40"
            },


         ]

      }

   }
   toggle() {
   this.setState({
     shown: !this.state.shown
   });
 }
   onMove() {
          console.log("this ic clic");
      }


  render() {

    var shown = {
      display: this.state.shown ? "block" : "none"
    };

    var hidden = {
      display: this.state.shown ? "none" : "block"
    }

    return (
      <div>
      <ShowData/>
        <h3>Hello world</h3>
        <h2 style={ shown }>this.state.shown = true</h2>
				<h2 style={ hidden }>this.state.shown = false</h2>
				<button onClick={this.toggle.bind(this)}>Toggle</button>
        <button  onClick={this.onMove}>Click</button>
        <p>{this.name}</p>
        <table>
             <tbody>
                {this.state.data.map((person, i) => <TableRow key = {i}
                   data = {person} />)}
             </tbody>
          </table>

      </div>
    );
  }
}

class TableRow extends React.Component {
   render() {
      return (

         <tr>
            <td>{this.props.data.id}</td>
            <td>{this.props.data.name}</td>
            <td>{this.props.data.age}</td>
         </tr>

      );
   }
}

class ShowData extends  React.Component{

  render(){
return(
    <div>
    <h3>This is other Component</h3>
    </div>
  )
  }
}
  export default Game;
