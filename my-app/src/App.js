import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Game from './home.js';
import Test from './test.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Game/>
        <Test/>
       </div>
    );
  }
}

export default App;
